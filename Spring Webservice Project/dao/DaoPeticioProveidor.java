package dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import projecte.PeticioProveidor;
import projecte.Producte;
import projecte.Proveidor;

public class DaoPeticioProveidor extends GenericDao<PeticioProveidor,Integer> implements IDaoPeticioProveidor {

	@Override
	public boolean afegirProducteAPeticio(PeticioProveidor p, Producte prod, boolean trans) {
		Session session = sessionFactory.getCurrentSession();
		p.setProducte(prod);
		try {
			if(trans)
				session.beginTransaction();
			
			session.saveOrUpdate(p);
			
			if(trans)
				session.getTransaction().commit();
			return true;
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();
			return false;
		}
	}

	
}
