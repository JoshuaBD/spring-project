package dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import projecte.Comanda;
import projecte.Producte;

public class DaoComanda extends GenericDao<Comanda,Integer> implements IDaoComanda {

	public boolean afegirComandaProducte(Comanda c, Producte p) {
		Session session = sessionFactory.getCurrentSession();
		p.getComandes().add(c);
		c.getProducte().add(p);
		try {
			session.beginTransaction();
			session.saveOrUpdate(c);
			session.saveOrUpdate(p);
			
			session.getTransaction().commit();
			return true;
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();
			return false;

		}
	}
	
}
