package dao;

import java.util.List;

import projecte.Comanda;
import projecte.Producte;

public interface IDaoComanda extends IGenericDao<Comanda,Integer> {

	void saveOrUpdate(Comanda c);

	Comanda get(Integer id);

	List<Comanda> list();

	void delete(Integer id);
	
	boolean afegirComandaProducte(Comanda c, Producte p);
}
