package dao;

import java.util.List;

import projecte.PeticioProveidor;
import projecte.Proveidor;

public interface IDaoProveidor extends IGenericDao<Proveidor,Integer>{

	void saveOrUpdate(Proveidor p);

	Proveidor get(Integer id);

	List<Proveidor> list();

	void delete(Integer id);
	
	boolean afegirPeticio(Proveidor p, PeticioProveidor pp);

}
