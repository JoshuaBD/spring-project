package dao;

import java.util.List;

import projecte.Lot;

public interface IDaoLot extends IGenericDao<Lot,Integer>{

	void saveOrUpdate(Lot l);

	Lot get(Integer id); //Este no

	List<Lot> list(); //Este no

	void delete(Integer id);
}
