package dao;

import java.util.List;

import projecte.PeticioProveidor;
import projecte.Producte;
import projecte.Proveidor;

public interface IDaoPeticioProveidor extends IGenericDao<PeticioProveidor,Integer>{

	void saveOrUpdate(PeticioProveidor p);

	PeticioProveidor get(Integer id);

	List<PeticioProveidor> list();

	void delete(Integer id);

	boolean afegirProducteAPeticio(PeticioProveidor p, Producte prod, boolean trans);

}
