package dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import projecte.Lot;
import projecte.PeticioProveidor;
import projecte.Producte;

public class DaoProducte extends GenericDao<Producte, Integer> implements IDaoProducte {

	public boolean afegirComponent(Producte p1, Producte p2) {
		Session session = sessionFactory.getCurrentSession();
		p1.getProductes().add(p2);
		try {
			session.beginTransaction();
			session.saveOrUpdate(p1);
			session.saveOrUpdate(p2);

			session.getTransaction().commit();
			return true;
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();
			return false;

		}
	}

	public boolean restarStock(Producte p, int quant) {
		Session session = sessionFactory.getCurrentSession();
		InterceptorStock.switchOnInterceptor();
		try {
			session.beginTransaction();

			Lot lot;
			if (p.getStock() >= quant) {
				for (Lot l : p.getLots()) {
					if (l.getQuantitat() > 0 && (l.getQuantitat() - quant) >= 0) {
						l.setQuantitat(l.getQuantitat() - quant);
						lot = l;
						p.setStock(p.getStock() - quant);
						session.saveOrUpdate(p);
						session.saveOrUpdate(lot);
						break;
					}
				}
			}

			session.getTransaction().commit();
			return true;
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();
			return false;

		} finally {
			InterceptorStock.switchOffInterceptor();
		}
	}

	@Override
	public boolean afegirPeticio(PeticioProveidor p, Producte prod, boolean trans) {
		Session session = sessionFactory.getCurrentSession();
		prod.getPeticionsProveidor().add(p);
		try {
			if (trans)
				session.beginTransaction();

			session.saveOrUpdate(prod);

			if (trans)
				session.getTransaction().commit();

			return true;
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();
			return false;

		}
	}

}
