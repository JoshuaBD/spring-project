package dao;

import java.util.List;

import projecte.Address;

public interface IDaoAddress extends IGenericDao<Address,Integer>{

	void saveOrUpdate(Address a);

	Address get(Integer id);

	List<Address> list();

	void delete(Integer id);

}
