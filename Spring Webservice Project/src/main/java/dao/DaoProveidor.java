package dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import projecte.PeticioProveidor;
import projecte.Proveidor;

public class DaoProveidor extends GenericDao<Proveidor,Integer> implements IDaoProveidor {

	public boolean afegirPeticio(Proveidor p, PeticioProveidor pp) {
		Session session = sessionFactory.getCurrentSession();
		p.getPeticions().add(pp);
		try {
			session.beginTransaction();
			session.saveOrUpdate(p);
			session.saveOrUpdate(pp);
			
			session.getTransaction().commit();
			return true;
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();
			return false;

		}
	}
}
