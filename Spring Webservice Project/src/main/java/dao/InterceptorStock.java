package dao;

import java.io.Serializable;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.EmptyInterceptor;
import org.hibernate.Transaction;
import org.hibernate.type.Type;

import projecte.PeticioProveidor;
import projecte.Producte;

public class InterceptorStock extends EmptyInterceptor {

	private static final long serialVersionUID = 1L;
	public static boolean interceptorEnabled = false;
	// Define a static logger
	// private static Logger logger =
	// LogManager.getLogger(LoggingInterceptor.class);
	
	public static void switchOffInterceptor() {
		interceptorEnabled = false;
	}
	
	public static void switchOnInterceptor() {
		interceptorEnabled = true;
	}
	
	@Override
	public boolean onFlushDirty(Object entity, Serializable id, Object[] currentState, Object[] previousState,
			String[] propertyNames, Type[] types) {
		if (entity instanceof Producte && interceptorEnabled) {
			System.out.println("\n\n");
			System.out.println("-------------onFlushDirty method is called.---------------");
			Producte prod = (Producte) entity;
			int stockActual = prod.getStock();
			int stockMinim = prod.getStockMinim();
			
			if (stockActual < stockMinim) {
				System.out.println(stockActual);
				System.out.println(stockMinim);
				System.out.println("\nSTOCK ACTUAL PER SOTA DE STOCK MINIM!");
				int quantitat = (stockMinim - stockActual) + 10; //Fem la diferencia +10 per tenir un marge
				PeticioProveidor peticio = new PeticioProveidor(1, new Date(), new Date(), "Pendent", quantitat);
				
				System.out.println("\nS'ha efectuat una peticio per a aquest producte\n");
				DaoProducte DProducte = new DaoProducte();
				DProducte.afegirPeticio(peticio, prod, false);
				
				DaoPeticioProveidor DPProveidor = new DaoPeticioProveidor();
				DPProveidor.afegirProducteAPeticio(peticio, prod, false);
				
			}
			System.out.println("------------------------------------------------");
		}
		return false;
	}

	@Override
	public boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {
		/*System.out.println("onSave method is called.");
		System.out.println("\n\n");
		System.out.println("STATE--------------");
		for(int i = 0;i < state.length;i++) {
			System.out.println(state[i]);
		}
		System.out.println("PROPERTY NAMES-------------");
		for(int i = 0;i < propertyNames.length;i++) {
			System.out.println(propertyNames[i]);
		}
		System.out.println("TYPES-------------");
		for(int i = 0;i < types.length;i++) {
			System.out.println(types[i]);
		}
		System.out.println("\n\n");
		
		if (entity instanceof Producte && !switchOff) {
			Producte prod = (Producte) entity;
			int stockActual = prod.getStock();
			int stockMinim = prod.getStockMinim();
			
			if (stockActual < stockMinim) {
				switchOff = true;
				System.out.println(stockActual);
				System.out.println(stockMinim);
				System.out.println("\n\n\nSTOCK ACTUAL PER SOTA DE STOCK MINIM!\n\n\n");
				int quantitat = (stockMinim - stockActual) + 10; //Fem la diferencia +10 per tenir un marge
				PeticioProveidor peticio = new PeticioProveidor(1, new Date(), new Date(), "Pendent", quantitat);
				
				System.out.println("\n\n\nS'ha efectuat una peticio per a aquest producte\n\n\n");
				DaoProducte DProducte = new DaoProducte();
				DProducte.afegirPeticio(peticio, prod, false);
				
				DaoPeticioProveidor DPProveidor = new DaoPeticioProveidor();
				DPProveidor.afegirProducteAPeticio(peticio, prod, false);
				
				
				
			}
		}*/
		return false;
	}

	@Override
	public boolean onLoad(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {
		/*
		 * System.out.println("onLoad method is called."); System.out.println(id); if
		 * (entity instanceof Producte) { Producte p = (Producte) entity;
		 * System.out.println(p.getNomProducte()); int stockActual = p.getStock(); int
		 * stockMinim = p.getStockMinim(); for (Object o: state) {
		 * System.out.println(o); } System.out.println(stockActual);
		 * System.out.println(stockMinim);
		 * 
		 * 
		 * if(stockActual < stockMinim) { System.out.println("\n \n \n Producte " +
		 * p.getNomProducte() + " amb codi Producte " + p.getCodiProducte() +
		 * ", té un stock menor al seu stock mínim!!!!"); }
		 * 
		 * 
		 * 
		 * }
		 */
		return super.onLoad(entity, id, state, propertyNames, types);

	}

	@Override
	public String onPrepareStatement(String sql) {
		// logger.info(sql);
		return super.onPrepareStatement(sql);
	}

}