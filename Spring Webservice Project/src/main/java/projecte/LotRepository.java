package projecte;

import java.util.List;

import org.springframework.data.repository.CrudRepository;




public interface LotRepository extends CrudRepository<Lot, Integer> {

}
