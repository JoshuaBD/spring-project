package projecte;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "peticions_proveidors")
public class PeticioProveidor implements Serializable{
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_peticio")
	protected int idPeticio;
	
	@Column(name = "tipus_peticio")
	protected int tipusPeticio;
	
	@Column(name = "data_ordre", nullable = false)
	protected Date dataOrdre;
	
	@Column(name = "data_recepcio", nullable = false)
	protected Date dataRecepcio;
	
	@Column(name = "estat")
	protected String estat;
	
	@Column(name = "quantitat")
	protected int quantitat;
	
	//relacio n a 1 amb proveidor
	@ManyToOne
	@JoinColumn(name="proveidor")
	private Proveidor proveidor;
	
	//relacio n a 1 amb producte
	@ManyToOne
	@JsonBackReference
	@JoinColumn(name="codi_producte")
	private Producte producte;
	
	public PeticioProveidor() {
		super();
	}
	
	public PeticioProveidor(int tipusPeticio, Date dataOrdre, Date dataRecepcio, String estat, int quantitat) {
		super();
		this.tipusPeticio = tipusPeticio;
		this.dataOrdre = dataOrdre;
		this.dataRecepcio = dataRecepcio;
		this.estat = estat;
		this.quantitat = quantitat;
	}

	public int getIdPeticio() {
		return idPeticio;
	}

	public void setIdPeticio(int idPeticio) {
		this.idPeticio = idPeticio;
	}

	public int getTipusPeticio() {
		return tipusPeticio;
	}

	public void setTipusPeticio(int tipusPeticio) {
		this.tipusPeticio = tipusPeticio;
	}

	public Date getDataOrdre() {
		return dataOrdre;
	}

	public void setDataOrdre(Date dataOrdre) {
		this.dataOrdre = dataOrdre;
	}

	public Date getDataRecepcio() {
		return dataRecepcio;
	}

	public void setDataRecepcio(Date dataRecepcio) {
		this.dataRecepcio = dataRecepcio;
	}

	public String getEstat() {
		return estat;
	}

	public void setEstat(String estat) {
		this.estat = estat;
	}

	public Proveidor getProveidor() {
		return proveidor;
	}

	public void setProveidor(Proveidor proveidor) {
		this.proveidor = proveidor;
	}

	public Producte getProducte() {
		return producte;
	}

	public void setProducte(Producte producte) {
		this.producte = producte;
	}

	public int getQuantitat() {
		return quantitat;
	}

	public void setQuantitat(int quantitat) {
		this.quantitat = quantitat;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 3;
		result = prime * result + idPeticio;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof PeticioProveidor))
			return false;
		PeticioProveidor other = (PeticioProveidor) obj;
		if (idPeticio != other.idPeticio)
			return false;
		return true;
	}
}
