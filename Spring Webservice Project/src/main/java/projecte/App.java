package projecte;

import java.util.Date;
import java.util.HashSet;
import java.util.List;

import dao.DaoAddress;
import dao.DaoClient;
import dao.DaoComanda;
import dao.DaoLot;
import dao.DaoPeticioProveidor;
import dao.DaoProducte;
import dao.DaoProveidor;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

public class App {
	public static void main(String[] args) {
		
		Address address = new Address(true, "Calle Guapeton", 1, "Sabadell", "España", "43003", "787878789", 0.0, 0.0);
		Client client = new Client("46381334n", "Dani", true, address);
		Producte prod = new Producte("Pastelitos", 0, 1000, UnitatMesura.UNITAT, Tipus.VENDIBLE, 100);
		Producte prod2 = new Producte("Comp1", 0, 1, UnitatMesura.GRAMS, Tipus.INGREDIENT, 0);
		Proveidor prov = new Proveidor("Prov1", "akijsnd", true, "Tienda", "Dani", address);
		//PeticioProveidor peticio = new PeticioProveidor(1, new Date(), new Date(), "En proces", 10);
		Lot lot = new Lot(1000, new Date(), new Date());
		Comanda comanda = new Comanda(new Date(), new Date(), ComandaEstat.LLIURADA, client);
		
		lot.setProducte(prod);
		prod.getLots().add(lot);
		
		DaoAddress DAddress = new DaoAddress();
		DaoClient DClient = new DaoClient();
		DaoProducte DProducte = new DaoProducte();
		DaoProveidor DProveidor = new DaoProveidor();
		DaoPeticioProveidor DPeticioProv = new DaoPeticioProveidor();
		DaoLot DLot = new DaoLot();
		DaoComanda DComanda = new DaoComanda();
		
		DAddress.saveOrUpdate(address);
		DClient.saveOrUpdate(client);
		
		DProducte.saveOrUpdate(prod);
		DProducte.saveOrUpdate(prod2);
		DProducte.afegirComponent(prod, prod2);
		
		DProveidor.saveOrUpdate(prov);
		//DPeticioProv.saveOrUpdate(peticio);
		//DProveidor.afegirPeticio(prov, peticio);
		DLot.saveOrUpdate(lot);
		DComanda.saveOrUpdate(comanda);
		
		/* ---------- EXERCICI 6 ---------- */
		
		/*Comanda comandaEx6 = new Comanda(new Date(), new Date(), ComandaEstat.PENDENT, client);
		
		DComanda.saveOrUpdate(comandaEx6);
		Producte p = DProducte.get(1);
		
		DComanda.afegirComandaProducte(comandaEx6, p);
		DProducte.restarStock(p, 1);*/
		
	}
}
