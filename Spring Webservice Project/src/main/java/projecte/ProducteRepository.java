package projecte;

import java.util.List;

import org.springframework.data.repository.CrudRepository;



public interface ProducteRepository extends CrudRepository<Producte, Integer> {
	List<Producte> findByNomProducteOrderByPreuVendaDesc(String nom);
}
