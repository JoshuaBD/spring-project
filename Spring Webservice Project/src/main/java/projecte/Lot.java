package projecte;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "Lots")
public class Lot implements Serializable {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_lot")
	int idLot;
	
	@Column(name = "quantitat")
	int quantitat;
	
	@Column(name = "data_entrada", nullable = true)
	Date dataEntrada = new Date();
	
	@Column(name = "data_caducitat", nullable = true)
	Date dataCaducitat = null;

	//relacio n a 1 amb producte
	@ManyToOne
	@JoinColumn(name="producte")
	@JsonBackReference
	private Producte producte;
	
	public Lot(){
		super();
	}
	
	public Lot(int quantitat, Producte prod) {
		super();
		this.quantitat = quantitat;
		this.dataEntrada = new Date();
		
		Calendar c = new GregorianCalendar();
		c.add(Calendar.DATE, 30);
		Date d = c.getTime();
		this.dataCaducitat = d;
		this.producte = prod;
	}
	
	public Lot(int quantitat, Date dataEntrada, Date dataCaducitat) {
		super();
		this.quantitat = quantitat;
		this.dataEntrada = dataEntrada;
		this.dataCaducitat = dataCaducitat;
	}
	
	public int getIdLot() {
		return idLot;
	}

	public void setIdLot(int idLot) {
		this.idLot = idLot;
	}

	public int getQuantitat() {
		return quantitat;
	}

	public void setQuantitat(int quantitat) {
		this.quantitat = quantitat;
	}

	public Date getDataEntrada() {
		return dataEntrada;
	}

	public void setDataEntrada(Date dataEntrada) {
		this.dataEntrada = dataEntrada;
	}

	public Date getDataCaducitat() {
		return dataCaducitat;
	}

	public void setDataCaducitat(Date dataCaducitat) {
		this.dataCaducitat = dataCaducitat;
	}
	
	public Producte getProducte() {
		return producte;
	}

	public void setProducte(Producte producte) {
		this.producte = producte;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idLot;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Lot))
			return false;
		Lot other = (Lot) obj;
		if (idLot != other.idLot)
			return false;
		return true;
	}
}