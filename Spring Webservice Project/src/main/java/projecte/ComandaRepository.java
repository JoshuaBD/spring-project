package projecte;

import org.springframework.data.repository.CrudRepository;

public interface ComandaRepository extends CrudRepository<Comanda, Integer> {
	
}
