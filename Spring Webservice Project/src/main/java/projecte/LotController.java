package projecte;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller //Esto significa que esta clase es un controlador
@RequestMapping(path="/lot")//Esto significa que la URL comienza con / demo (después de la ruta de la aplicación)
public class LotController {
	@Autowired
	private LotRepository repository;
	
	@Autowired
	private ProducteRepository repositoryProd;
	
	@GetMapping(path="/add") //EL MAPEO SOLO OBTIENE SOLICITUDES
	public @ResponseBody String addNewLot (@RequestParam int quant, @RequestParam int idProducte) {
		//@ResponseBody significa que la cadena devuelta es la respuesta, no un nombre de vista
		//@RequestParam significa que es un parámetro de la solicitud GET o POST
		
		Optional<Producte> p = repositoryProd.findById(idProducte);
		Lot lot = new Lot(quant, p.get());
		
		repository.save(lot);
		
		return "saved";
	}
	
	@GetMapping(path="/delete")
	public @ResponseBody String deleteLot(@RequestParam int id) {
		repository.deleteById(id);
		return "deleted";
	}
	
	@GetMapping(path="/{id}")
	public @ResponseBody Lot getLot(@PathVariable int id) {
		Optional<Lot> lot = repository.findById(id);
		return lot.get();
	}
	
	@GetMapping(path = "/all")
	public @ResponseBody Iterable<Lot> getAllLots() {
		return repository.findAll();
	}
}
