package projecte;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
@Table(name = "Clients")
public class Client implements Serializable{

	@Id
	@Column(name = "nif", length = 9)
	private String nif;

	@Column(name = "nombre")
	protected String nomClient;
	
	@Type(type="numeric_boolean")
	@Column(name = "actiu")
	protected boolean actiu;

	// OneToOne, simplemente pones el objeto al que referencias
	@JoinColumn(name = "direccio", nullable = false)
	@OneToOne(cascade = CascadeType.PERSIST)
	private Address address;
	
	//relacio 1 a n amb Comanda
	@OneToMany(mappedBy = "client")
	Set<Comanda> comandes = new HashSet<Comanda>();

	public Client() {
		super();
	}
	//constructor sin comanddes
	public Client(String nIF, String nomClient, boolean actiu, Address address) {
		super();
		nif = nIF;
		this.nomClient = nomClient;
		this.actiu = actiu;
		this.address = address;
	}
	public Client(String nIF, String nomClient, boolean actiu, Address address, Set<Comanda> comandes) {
		super();
		nif = nIF;
		this.nomClient = nomClient;
		this.actiu = actiu;
		this.address = address;
		this.comandes = comandes;
	}

	public String getNIF() {
		return nif;
	}

	public void setNIF(String nIF) {
		nif = nIF;
	}

	public String getNomClient() {
		return nomClient;
	}

	public void setNomClient(String nomClient) {
		this.nomClient = nomClient;
	}

	public boolean isActiu() {
		return actiu;
	}

	public void setActiu(boolean actiu) {
		this.actiu = actiu;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Set<Comanda> getComandes() {
		return comandes;
	}

	public void setComandes(Set<Comanda> comandes) {
		this.comandes = comandes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 6;
		result = prime * result + ((nif == null) ? 0 : nif.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Client other = (Client) obj;
		if (nif == null) {
			if (other.nif != null)
				return false;
		} else if (!nif.equals(other.nif))
			return false;
		return true;
	}
}