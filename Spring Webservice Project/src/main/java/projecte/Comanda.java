package projecte;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "Comandes")
public class Comanda implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_comanda")
	protected int idComanda;

	@Column(name = "data_comanda")
	protected Date dataComanda;
	
	@Column(name = "data_lliurament")
	protected Date dataLliurament;

	@Enumerated(EnumType.STRING)
	@Column(name = "estat")
	protected ComandaEstat estat;
	
	// relacio n a n amb producte
	@ManyToMany(cascade = {CascadeType.ALL},mappedBy="comandes", fetch = FetchType.EAGER)
	private Set<Producte> producte= new HashSet<Producte>();
	
	// relacio n a 1 amb client
	@ManyToOne
	@JsonBackReference
	@JoinColumn(name = "nif")
	private Client client;

	public Comanda(Date dataComanda, Date dataLliurament, ComandaEstat estat, Client client) {
		super();
		this.dataComanda = dataComanda;
		this.dataLliurament = dataLliurament;
		this.estat = estat;
		this.client = client;
	}
	
	public Comanda(ComandaEstat estat, Client client) {
		super();
		this.dataComanda = new Date();
		
		Calendar c = new GregorianCalendar();
		c.add(Calendar.DATE, 30);
		Date d = c.getTime();
		this.dataLliurament = d;
		this.estat = estat;
		this.client = client;
	}

	public Comanda() {
		super();
	}

	public int getIdComanda() {
		return idComanda;
	}

	public void setIdComanda(int idComanda) {
		this.idComanda = idComanda;
	}

	public Date getDataComanda() {
		return dataComanda;
	}

	public void setDataComanda(Date dataComanda) {
		this.dataComanda = dataComanda;
	}

	public Date getDataLliurament() {
		return dataLliurament;
	}

	public void setDataLliurament(Date dataLliurament) {
		this.dataLliurament = dataLliurament;
	}

	public ComandaEstat getEstat() {
		return estat;
	}

	public void setEstat(ComandaEstat estat) {
		this.estat = estat;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Set<Producte> getProducte() {
		return producte;
	}

	public void setProducte(Set<Producte> producte) {
		this.producte = producte;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 7;
		result = prime * result + idComanda;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Comanda))
			return false;
		Comanda other = (Comanda) obj;
		if (idComanda != other.idComanda)
			return false;
		return true;
	}
	
	
	
}