package projecte;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping(path="/comanda")
public class ComandaController {
	@Autowired
	private ComandaRepository repositoryCom;
	
	@Autowired
	private ProducteRepository repositoryProd;
	
	@Autowired
	private ClientRepository repositoryClient;
	
	@GetMapping(path="/create")
	public @ResponseBody String createNewComanda (@RequestParam String estat, @RequestParam int[] idProd, @RequestParam String nif) {
		Set<Producte> prods = new HashSet<Producte>();
		
		for(int i = 0;i < idProd.length;i++) {
			Optional<Producte> p = repositoryProd.findById(idProd[i]);
			prods.add(p.get());
		}
		
		List<Client> cl = repositoryClient.findByNifIgnoreCase(nif);
		
		estat = estat.toUpperCase();
		ComandaEstat estatCom;
		
		switch(estat) {
		case "LLIURADA":
			estatCom = ComandaEstat.LLIURADA;
			break;
			
		case "PENDENT":
			estatCom = ComandaEstat.PENDENT;
			break;
			
		case "PREPARADA":
			estatCom = ComandaEstat.PREPARADA;
			break;
			
		case "TRANSPORT":
			estatCom = ComandaEstat.TRANSPORT;
			break;
			
		default:
			estatCom = ComandaEstat.PENDENT;
			break;
		}
		
		
		Comanda c = new Comanda(estatCom, cl.get(0));
		c.setProducte(prods);
		
		repositoryCom.save(c);
		
		for(Producte prod: prods) {
			prod.getComandes().add(c);
			repositoryProd.save(prod);
		}
		
		
		
		
		return "La id de la comanda creada és: " +Integer.toString(c.getIdComanda());
	}
	
	@GetMapping(path="/{id}")
	public @ResponseBody Comanda getComanda(@PathVariable int id) {
		Optional<Comanda> comanda = repositoryCom.findById(id);
		return comanda.get();
	}
	
	@GetMapping(path="/{id}/estat")
	public @ResponseBody String getEstatComanda(@PathVariable int id) {
		Optional<Comanda> comanda = repositoryCom.findById(id);
		return "El estat de la comanda amb id " + id + " és " + comanda.get().getEstat().toString();
	}
	
	@GetMapping(path = "/all")
	public @ResponseBody Iterable<Comanda> getAllLots() {
		return repositoryCom.findAll();
	}
}
