package projecte;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller //Esto significa que esta clase es un controlador
@RequestMapping(path="/producte")//Esto significa que la URL comienza con / demo (después de la ruta de la aplicación)
public class ProducteController {
	@Autowired //esto significa que cojere el repositorio llamado cmo la siguiente linea
	//El cual es generado automáticamente por Spring, lo usaremos para manejar los datos
	private ProducteRepository repository;
	
	@GetMapping(path="/{nomProducte}")
	public @ResponseBody Producte getProducte(@PathVariable String nomProducte) {
		
		List<Producte> lista = repository.findByNomProducteOrderByPreuVendaDesc(nomProducte);
		
		return lista.get(0);
	}
	
	@GetMapping(path = "/all")
	public @ResponseBody Iterable<Producte> getAllProductes() {
		// This returns a JSON or XML with the users
		return repository.findAll();
	}
	
	
	
}
