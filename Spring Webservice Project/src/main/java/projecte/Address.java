package projecte;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "Addresses")
public class Address implements Serializable{
	//NO SE SI ES ASI, PERO SE ENCONTRARA POR ID LA DIRECCION
	//DE CLIENTES I DE PROVEEDORES, NO PODRAN TENER LA MISMA...
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_address")
	protected int id;
	
	@Column(name = "principal")
	protected boolean principal;
	
	@Column(name = "carrer", nullable = false)
	protected String carrer;
	
	@Column(name = "numero")
	protected int numero;
	
	@Column(name = "poblacio")
	protected String poblacio;
	
	@Column(name = "pais")
	protected String pais;
	
	@Column(name = "postal")
	protected String postal;
	
	@Column(name = "telefon", length = 9)
	protected String telefon;
	
	@Column(name = "latitud")
	protected double latitud;
	
	@Column(name = "longitud")
	protected double longitud;
	
	public Address() {
		super();
	}
	
	public Address(boolean principal, String carrer, int numero, String poblacio, String pais, String postal,
			String telefon, double latitud, double longitud) {
		super();
		this.principal = principal;
		this.carrer = carrer;
		this.numero = numero;
		this.poblacio = poblacio;
		this.pais = pais;
		this.postal = postal;
		this.telefon = telefon;
		this.latitud = latitud;
		this.longitud = longitud;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isPrincipal() {
		return principal;
	}

	public void setPrincipal(boolean principal) {
		this.principal = principal;
	}

	public String getCarrer() {
		return carrer;
	}

	public void setCarrer(String carrer) {
		this.carrer = carrer;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getPoblacio() {
		return poblacio;
	}

	public void setPoblacio(String poblacio) {
		this.poblacio = poblacio;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getPostal() {
		return postal;
	}

	public void setPostal(String postal) {
		this.postal = postal;
	}

	public String getTelefon() {
		return telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	public double getLatitud() {
		return latitud;
	}

	public void setLatitud(double latitud) {
		this.latitud = latitud;
	}

	public double getLongitud() {
		return longitud;
	}

	public void setLongitud(double longitud) {
		this.longitud = longitud;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 5;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Address))
			return false;
		Address other = (Address) obj;
		if (id != other.id)
			return false;
		return true;
	}	
}