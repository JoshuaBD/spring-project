package projecte;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "Productes")
public class Producte implements Serializable{
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "codi_producte")
	protected int codiProducte;
	
	@Column(name = "nom_producte")
	protected String nomProducte;
	
	@Column(name = "stock", nullable = false, updatable = true)
	protected int stock;
	
	@Column(name = "stock_minim", nullable = false, updatable = true)
	protected int stockMinim;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "unitat")
	protected UnitatMesura unitat;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "tipus")
	protected Tipus tipus;
	
	@Column(name = "preu_venda", nullable = false, updatable = true)
	protected double preuVenda;
	
	//relacio 1 a n amb lot
	@OneToMany(mappedBy="producte", fetch = FetchType.EAGER)
	@JsonManagedReference
	Set<Lot> lots = new HashSet<Lot>();
	
	//relacio n a n amb ell mateix
	@ManyToMany(cascade = {CascadeType.REFRESH}, targetEntity = projecte.Producte.class)
	@JoinTable(name="ProducteAProducte", joinColumns={@JoinColumn(name="codi_producte")}, inverseJoinColumns={@JoinColumn(name="codi_component")})
	@JsonBackReference
	private Set<Producte> productes = new HashSet<Producte>();
	
	//relacio 1 a n amb peticionsproveidor
	@OneToMany(mappedBy="producte")
	Set<PeticioProveidor> peticionsProveidor = new HashSet<PeticioProveidor>();
	
	//relacio n a n amb comanda
	@ManyToMany(cascade = {CascadeType.REFRESH}, fetch = FetchType.EAGER)
	@JoinTable(name="comanda_producte", joinColumns={@JoinColumn(name="codi_producte")}, inverseJoinColumns={@JoinColumn(name="id_comanda")})
	@JsonBackReference
	private Set<Comanda> comandes = new HashSet<Comanda>();
	
	public Producte() {
		super();
	}
	
	public Producte(String nomProducte, int stock, int stockMinim, UnitatMesura unitat, Tipus tipus, double preuVenda) {
		super();
		this.nomProducte = nomProducte;
		this.stock = stock;
		this.stockMinim = stockMinim;
		this.unitat = unitat;
		this.tipus = tipus;
		this.preuVenda = preuVenda;
	}

	public int getCodiProducte() {
		return codiProducte;
	}

	public void setCodiProducte(int codiProducte) {
		this.codiProducte = codiProducte;
	}

	public String getNomProducte() {
		return nomProducte;
	}

	public void setNomProducte(String nomProducte) {
		this.nomProducte = nomProducte;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public int getStockMinim() {
		return stockMinim;
	}

	public void setStockMinim(int stockMinim) {
		this.stockMinim = stockMinim;
	}

	public UnitatMesura getUnitat() {
		return unitat;
	}

	public void setUnitat(UnitatMesura unitat) {
		this.unitat = unitat;
	}

	public Tipus getTipus() {
		return tipus;
	}

	public void setTipus(Tipus tipus) {
		this.tipus = tipus;
	}

	public double getPreuVenda() {
		return preuVenda;
	}

	public void setPreuVenda(double preuVenda) {
		this.preuVenda = preuVenda;
	}

	public Set<Comanda> getComandes() {
		return comandes;
	}

	public void setComandes(Set<Comanda> comandes) {
		this.comandes = comandes;
	}

	public Set<Lot> getLots() {
		return lots;
	}

	public void setLots(Set<Lot> lots) {
		this.lots = lots;
	}

	public Set<Producte> getProductes() {
		return productes;
	}

	public void setProductes(Set<Producte> productes) {
		this.productes = productes;
	}

	public Set<PeticioProveidor> getPeticionsProveidor() {
		return peticionsProveidor;
	}

	public void setPeticionsProveidor(Set<PeticioProveidor> peticionsProveidor) {
		this.peticionsProveidor = peticionsProveidor;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 2;
		result = prime * result + codiProducte;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Producte))
			return false;
		Producte other = (Producte) obj;
		if (codiProducte != other.codiProducte)
			return false;
		return true;
	}	
}